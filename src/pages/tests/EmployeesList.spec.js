import { shallowMount } from '@vue/test-utils';
import '@babel/polyfill';

import EmployeesList from '../EmployeesList.vue';

jest.mock('axios', () => ({
    get: () => Promise.resolve([]),
}));

test('is a Vue instance', () => {
    const wrapper = shallowMount(EmployeesList);
    expect(wrapper.isVueInstance()).toBeTruthy();
})


test('renders correctly', () => {
    const wrapper = shallowMount(EmployeesList);
    wrapper.setMethods({ fetchData: () => { } })
    wrapper.setData({
        loading: false,
        employees: [
            {
                name: 'xxx',
                phone: '125-567',
                address: {
                    street: 'Mouse Street',
                    suite: 'Suite 15',
                    city: 'New Danton'
                }, 
                id: 12,
                email : 'xxx@xxx.xxx'


            },
            {
                name: 'zzz',
                phone: '259-987',
                address: {
                    street: 'Cat Street',
                    suite: 'Suite 157',
                    city: 'New City'
                }, 
                id: 17,
                email : 'zzz@zzz.zzz'


            }
        ]
    })
    expect(wrapper.element).toMatchSnapshot();
})

